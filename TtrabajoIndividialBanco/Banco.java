package TtrabajoIndividialBanco;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Banco {

    public static void main(String[]args){

        CuentaCorriente c1 = new CuentaCorriente("Alfredo", 25000, 15555880);
        CuentaCorriente c2 = new CuentaCorriente("Rocio", 30000, 15555881);

        Set<CuentaCorriente> listadoDeCuentas = new HashSet<>();
        listadoDeCuentas.add(c1);
        listadoDeCuentas.add(c2);

        Operaciones.Transferir(c1, c2, 8000);

        System.out.println("Salida: "+listadoDeCuentas.toString());

        try {
            ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                    "C:\\Users\\Florencia\\IdeaProjects\\TPLaboratorio\\clonar\\src\\com\\demogitlab\\Laboratorio\\tplaboratorioii\\TtrabajoIndividialBanco\\listadoDeCuentas.dat"));

            flujoDeSalida.writeObject(listadoDeCuentas);
            flujoDeSalida.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                    "C:\\Users\\Florencia\\IdeaProjects\\TPLaboratorio\\clonar\\src\\com\\demogitlab\\Laboratorio\\tplaboratorioii\\TtrabajoIndividialBanco\\listadoDeCuentas.dat"));

            Set<CuentaCorriente> listadoDeCuentasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();

            System.out.println("Entrada: "+ listadoDeCuentasEntrada.toString());

            flujoDeEntrada.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

class CuentaCorriente implements Serializable{

    private String tTitular;
    private double saldo;
    private int nCuenta;

    public CuentaCorriente(String tTitular, double saldo, int nCuenta) {
        this.tTitular = tTitular;
        this.saldo = saldo;
        this.nCuenta = nCuenta;
    }

    public String gettTitular() {
        return tTitular;
    }

    public void settTitular(String tTitular) {
        this.tTitular = tTitular;
    }

    public double verSaldo() {
        return saldo;
    }

    public void ingresarCash(double cash) {
        this.saldo = this.saldo + cash;
    }

    public void sacarCash(double cash) {
        this.saldo = this.saldo - cash;
    }

    public int getnCuenta() {
        return nCuenta;
    }

    public void setnCuenta(int nCuenta) {
        this.nCuenta = nCuenta;
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "nombreTitular='" + tTitular + '\'' +
                ", saldo=" + saldo +
                ", nroCuenta=" + nCuenta +
                '}';
    }

}


class Operaciones{

    public static void Transferir(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto){
        cuentaEgreso.sacarCash(monto);
        cuentaIngreso.ingresarCash(monto);
    }

}