package Desafíos;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int edad;
        String nombre;
        double salario;
        boolean carnet = true;

        System.out.println("Ingrese su nombre: ");
        nombre = entrada.next();
        System.out.print("Indique su edad: ");
        edad = entrada.nextInt();
        System.out.println("Ingrese su remuneración pretendida: ");
        salario = entrada.nextDouble();

        System.out.println("Su nombre es " + nombre + ", tiene " + edad + " años y pretende un salario de $" + salario);
        if (carnet) {
            System.out.print("Su carnet fue registrado con éxito");

        }

    }
}

