package Desafíos;
public class UnEjercicioCombinado {
    public static void main(String[] args) {
        // realizar combinacion combinada (6/5 - 2/3 x 7/2 + 2/30)/ 1/3 / 5
        double numerador = ((6d/5d) - (2d/3d) *(7d/2d) + (2d/30d));
        double denominador = ((1d/3d)/5d);
        double resultado = numerador/denominador;
        System.out.println("El resultado de la operacion es: "+ numerador/denominador);
    }
}