package Desafíos;

public class UsandoLaApi {
    public static void main(String[] args) {
        // uso de:
        //Funciones trigonométricas habituales:
        // Math.sin, Math.cos, Math.tan, Math.atan, Math.atan2
        // La función exponencial y su inversa
        //Math.exp, Math.log
        //Las dos constantes PI y e
        //Math.PI, Math.E

        double ang = 45.0 * Math.PI / 180.0;
        double ang2 = 75.5;
        double ang3 = 30.5;
        double j = 25.0;
        double rdo = Math.exp(j);
        double loga = Math.log (j)/ Math.log (ang);

        System.out.println("El coseno de(" + 45 + ") es de " + Math.cos(ang));
        System.out.println("El seno de (" + 75.5 + ") es de " + Math.sin(ang));
        System.out.println("La tangente de(" + 30.5 + ") es de " + Math.tan(ang));
        System.out.println("El angulo theta es de " + Math.atan2(ang2, ang3));
        System.out.println("El arcotangente de (" + ang2 + ") es de " + Math.atan(ang3));
        System.out.println("El exponente de j es: "+ rdo);
        System.out.println("El resultado del logaritmo es: "+ loga);
        System.out.println("El logaritmo de E es " + Math.log(Math.E));


    }
}