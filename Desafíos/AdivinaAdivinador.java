package Desafíos;
import java.util.Scanner;

public class AdivinaAdivinador {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int num = (int) ((Math.random() * 100) + 1); //numero aleatorio del 1 al 100
        int u, i;

        System.out.println("El juego consiste en adivinar un número del 1 al 100.");

        System.out.print("Escribe un número entre 1 y 100: ");
        u = entrada.nextInt();

        for (i = 0; i <= 10; i++) {

            // Primero analizamos si acierta.

            if (num == u) {
                System.out.println("¡COOOOOORRECTOOOO!");
                break;
            }

            // Luego ver si acierta

            // Si no es ninguno de los casos anteriores, comparamos números.

            else if (num > u)
                System.out.println("El número es MAYOR que " + u);
            else if (num < u)
                System.out.println("El número es MENOR que " + u);
            //cantidad de oportunidades
            if (i <=100) {
                System.out.print("Intenta de nuevo, vos podes : ");
                u = entrada.nextInt(); }

        }
        System.out.print("Numero de intentos realizados: "+ i);
    }
}
