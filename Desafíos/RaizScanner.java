package Desafíos;

import java.util.Scanner;

public class RaizScanner {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        //elegir variable para guardar el numero ingresado
        int numraiz;
        //se pide el numero al usuario
        System.out.print("Introduzca un numero: ");
        numraiz = entrada.nextInt();
        //se realiza la raiz de dicho numero
        double resultado = Math.sqrt(numraiz);

        System.out.println("La raíz de " + numraiz + " es de: " + resultado);

    }
}
