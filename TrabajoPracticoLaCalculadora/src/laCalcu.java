import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class laCalcu {
    public static void main(String[] args) {
        mCalcu lemarco = new mCalcu();
        lemarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lemarco.setVisible(true);
    }


}

class mCalcu extends JFrame {
    public mCalcu(){
        setTitle("Calculadora");
        setBounds(600,400,300,250);
        laminaCalcu lalami = new laminaCalcu();
        add(lalami);
    }

}

class laminaCalcu extends JPanel {

    public laminaCalcu() {
        principio = true;
        setLayout(new BorderLayout());
        resultado = new JButton("0");
        resultado.setEnabled(false);

        add(resultado, BorderLayout.NORTH);

        botonescalcu = new JPanel();
        resultado.setBackground(Color.BLACK);
        botonescalcu.setLayout(new GridLayout(4, 4));

        ActionListener agregar = new agregaNum();
        ActionListener operacion = new operacionesMat();

        ponerBoton("7", agregar);
        ponerBoton("8", agregar);
        ponerBoton("9", agregar);
        ponerBoton("x", operacion);
        ponerBoton("4", agregar);
        ponerBoton("5", agregar);
        ponerBoton("6", agregar);
        ponerBoton("-", operacion);
        ponerBoton("1", agregar);
        ponerBoton("2", agregar);
        ponerBoton("3", agregar);
        ponerBoton("+", operacion);
        ponerBoton(",", agregar);
        ponerBoton("0", agregar);
        ponerBoton("=", operacion);
        ponerBoton("/", operacion);

        add(botonescalcu, BorderLayout.CENTER);

        ultimaOperacion = "=";
    }

    private void ponerBoton(String rotulo, ActionListener oyente) {

        JButton boton = new JButton(rotulo);
        boton.addActionListener(oyente);
        botonescalcu.add(boton);
        boton.setBackground(Color.BLACK);

    }
    private class agregaNum implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            String num = e.getActionCommand();

            if(principio){
                resultado.setText("");
                principio = false;
            }
            resultado.setText(resultado.getText() + num);
        }
    }
    private class operacionesMat implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            String opcalcu =e.getActionCommand();
            calcular(Double.parseDouble(resultado.getText()));

            ultimaOperacion = opcalcu;

            principio=true;
        }

        public void calcular(double x){

            switch (ultimaOperacion) {
                case "+":
                    operaResult += x;

                    break;
                case "-":
                    operaResult -= x;
                    break;
                case "x":
                    operaResult *= x;
                    break;
                case "/":
                    operaResult /= x;
                    break;
                case "=":
                    operaResult = x;
                    break;
            }

            resultado.setText(""+ operaResult);

        }
    }
    private JPanel botonescalcu;

    private JButton resultado;

    private boolean principio;

    private double operaResult;

    private String ultimaOperacion;

    }

