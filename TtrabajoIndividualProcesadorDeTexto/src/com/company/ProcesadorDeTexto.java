package com.company;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;

public class ProcesadorDeTexto {

    //-------------------PARTE PRINCIPAL--------------------

    public static void main(String[] args) {

        MenuProcesador elmarco = new MenuProcesador();
        //para que se cierre cuando la dejamos de usar
        elmarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        elmarco.setTitle("Procesador de Texto");
        elmarco.setLocationRelativeTo(null);
        elmarco.setVisible(true);
    }
}


class MenuProcesador extends JFrame {

    public MenuProcesador() {

        setBounds(800, 700, 500, 500);
        FondoProcesador fondo = new FondoProcesador();
        add(fondo);

    }
}


class FondoProcesador extends JPanel {
    private static JTextPane elarea;

    JMenu archivo = new JMenu("Archivo");

    JMenu fuente = new JMenu("Fuente");

    JMenu estilo = new JMenu("Estilo");

    JMenu tamagno = new JMenu("Tamaño");

    public FondoProcesador() {

        setLayout(new BorderLayout());

        JPanel fondomenu = new JPanel();

        //---------------BARRA DEL MENU--------------------------
        JMenuBar labarra = new JMenuBar();

        labarra.add(archivo);
        labarra.add(fuente);
        labarra.add(estilo);
        labarra.add(tamagno);
        //se le agrega color a la barra
        labarra.setBackground(new Color(176, 196, 222));

        fondomenu.add(labarra);
        add(fondomenu, BorderLayout.NORTH);
        //se le agrega color al fondo de la barra
        fondomenu.setBackground(new Color(176, 196, 222));

        elarea = new JTextPane();
        //se le agrega color a donde se escribe
        elarea.setBackground(new Color(238, 232, 170));

        //Se agrega el scroll para poder ver lo que se va escribiendo o lo que esta escrito
        JScrollPane scroll = new JScrollPane(elarea);

        add(labarra, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);


        //-----------------------ITEMS DE ARCHIVO--------------------------

        JMenuItem abrirjm = new JMenuItem("Abrir");

        JMenuItem guardarjm = new JMenuItem("Guardar");

        archivo.add(abrirjm);
        archivo.add(guardarjm);

        //-----------------------ITEMS DE LA FUENTE------------------------

        ConfigMenu("Arial", fuente);
        ConfigMenu( "Calibri", fuente);
        ConfigMenu("Tahoma", fuente);

        //----------------------ITEMS DEL ESTILO-----------------------------

        ConfigMenu("Negrita", estilo);
        ConfigMenu("Cursiva", estilo);

        //---------------------ITEMS DEL TAMAÑO------------------------------
        ConfigMenu("10", tamagno);
        ConfigMenu("12", tamagno);
        ConfigMenu("14", tamagno);
        ConfigMenu("20", tamagno);
        ConfigMenu("24", tamagno);

        //---------------------------------------------------------------------------

        abrirjm.addActionListener(new SeEscuchaAbrir());
        guardarjm.addActionListener(new EscucharGuardar());

    }
    //Método para cambiar las fuentes, estilos y tamaño de los textos
    public void ConfigMenu(String nombre, JMenu menu) {

        JMenuItem elegimenu = new JMenuItem(nombre);
        menu.add(elegimenu);

        {
            if (menu == fuente){
                elegimenu.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", nombre));
            }else if (menu == estilo){
                if(nombre.equalsIgnoreCase("Negrita")){
                    elegimenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                    elegimenu.addActionListener(new StyledEditorKit.BoldAction());

                }else if(nombre.equalsIgnoreCase("Cursiva")) {
                    elegimenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                    elegimenu.addActionListener((new StyledEditorKit.ItalicAction()));
                }
            }else if (menu == tamagno){
                elegimenu.addActionListener(new StyledEditorKit.FontSizeAction("tamaño", Integer.parseInt(nombre)));
            }
        }

    }

    private static String guardaRuta = "";

    //este boleano se ocupara para saber si el archivo nuevo fue guardado o no
    public static boolean guardate = false;

    //Metodo para cargar datos
    public static void cargarDatos () {
        try
        {
            JFileChooser elegirArchivo = new JFileChooser();

            elegirArchivo.showOpenDialog(elarea);

            //Direccion del archivo
            String archivo = elegirArchivo.getSelectedFile().getAbsolutePath();

            ObjectInputStream leyendoFichero = new ObjectInputStream(new FileInputStream(archivo));

            //Lectura del archivo
            elarea.setText((String) leyendoFichero.readObject());


            leyendoFichero.close();
            guardate = false;
        }
        catch (IOException ioe)
        {
            JOptionPane.showConfirmDialog(null, "SIN ACCESO", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("No se puede encontrar");
            c.printStackTrace();
        }

    }

    //Método de escucha del "botón" Abrir del menu Archivo
    private static class SeEscuchaAbrir implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            FondoProcesador.cargarDatos();
        }
    }

    //Método del "boton" Guardar
    public static void guardaTexto () {
        try
        {
            JFileChooser elegirArchivo = new JFileChooser();
            elegirArchivo.showSaveDialog(elarea);

            //Se verifica donde esta el archivo
            File archivo = elegirArchivo.getCurrentDirectory();

            String ruta = archivo+"\\"+elegirArchivo.getSelectedFile().getName();


            //Donde se guarda el archivo
            guardaRuta = ruta;

            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(ruta) );

            //Donde se guarda el texto
            escribiendoFichero.writeObject(elarea.getText());

            JOptionPane.showConfirmDialog(null, "LISTO.", "Éxito", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);
            guardate = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //Sobreescritura de archivos realizados
    public static void guardadoVeloz () {
        try
        {
            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(guardaRuta) );

            escribiendoFichero.writeObject(elarea.getText());

            escribiendoFichero.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //Metodo de escucha del "boton" Guardar del menu Archivos
    private static class EscucharGuardar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (guardate) {
                FondoProcesador.guardadoVeloz();
            } else {
                FondoProcesador.guardaTexto();
            }

        }
    }

}

