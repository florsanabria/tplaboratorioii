import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.*;


public class eventosDeFoco {
    public static void main(String[] args) {
        MarcoFoco lemarco = new MarcoFoco();
        lemarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}

class MarcoFoco extends JFrame{
    public MarcoFoco(){
    setVisible(true);
    setBounds(400,200,600,600);
    }
}

class LaminFoc extends JFrame{
    //como es la lamina
    public void paintComponent(Graphics g){
    super.paintComponents(g);
    setLayout(null);
    //El cuadro de texto dentro del marco
    c1 = new JTextField();
    c2 = new JTextField();
    c1.setBounds(100, 200, 150, 150);
    c2.setBounds(150, 250, 250, 250);
    //Agrego para que se vea en la lamina
    add(c1);

    add(c2);

    //para clase oyente
        EscuchoFoco leFoco = new EscuchoFoco();
        c2.addFocusListener(leFoco);
    }


private class EscuchoFoco implements FocusListener{

    @Override
    public void focusGained(FocusEvent e) {
        System.out.println("El elemento ha ganado el foco");

    }

    @Override
    public void focusLost(FocusEvent e) {

    }

}
    JTextField c1;

    JTextField c2;
}
