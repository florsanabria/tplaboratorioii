package Desafio11;

import java.util.Random;

public class CuentaCorriente {
    //Propiedades
    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    public CuentaCorriente(String nombreTitular, double saldo){
        this.nombreTitular = nombreTitular;
        this.saldo = saldo;
        //Num de cuenta aleatorio utilizando Random()
        Random nr = new Random();
        long aleatorio = nr.nextLong();
        if(aleatorio < 0){
            aleatorio = aleatorio * -1;
            numeroCuenta = aleatorio;
        }else{
            numeroCuenta = aleatorio;
        }
    }

    //Setters
    public void ponerDinero(double ingreso) {
        this.saldo = saldo + ingreso;
    }

    public void sacarDinero(double monto) {
        this.saldo = saldo - monto;
    }

    //Getters
    public double mostrarSaldo() {
        return saldo;
    }

    public String mostrarDatos() {
        return "Nombre del titular: " + nombreTitular + "\nNúmero de cuenta: " + numeroCuenta;
    }

    public boolean Transferencia(CuentaCorriente emisor, CuentaCorriente receptor, double cantidad) {
        boolean exitosa = true;
        if (cantidad < 0) {
             exitosa = false;
        } else if (saldo >= cantidad) {
            emisor.sacarDinero(cantidad);
            receptor.ponerDinero(cantidad);
        } else {
            exitosa = false;
        }
        return exitosa;
    }
}
