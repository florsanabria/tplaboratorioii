package Desafio11;

public class UsoCuenta {

    public static void main(String[] args) {
        //Instancias con las cuentas
        CuentaCorriente Cuenta1 = new CuentaCorriente("Rafael Castillo", 5500);
        CuentaCorriente Cuenta2 = new CuentaCorriente("Federico Rosales", 3600);

        System.out.println("");
        System.out.println(Cuenta1.mostrarDatos() + "\nSaldo antes de transferir: $" + Cuenta1.mostrarSaldo());
        System.out.println("");
        System.out.println(Cuenta2.mostrarDatos() + "\nSaldo antes de transferir: $" + Cuenta2.mostrarSaldo());

        System.out.println("");

        Cuenta1.Transferencia(Cuenta1, Cuenta2, 2500);
        //Impresión de datos en consola luego de la transferencia
        System.out.println(Cuenta1.mostrarDatos() +"\nSaldo actual: $" + Cuenta1.mostrarSaldo());
        System.out.println("");
        System.out.println(Cuenta2.mostrarDatos()+"\nSaldo actual: $" + Cuenta2.mostrarSaldo());
    }
}
